extends KinematicBody2D


# Declare member variables here. Examples:
var state
enum states {
	IDLE,
	WALKING,
	JUMPING,
	THROWING,
}
var throwTime = 0.5
var throwTimer = 0
var input = Vector2()
var velocity = Vector2()
var lastVelocity = Vector2()
var acceleration = 0
var inertia = Vector2()
const SPEED = 512
const ACCELERATION_SPEED = 1
const DEACCELERATION_SPEED = 0.5
const JUMP_FORCE = 768
const GRAVITY = 1568

var sprite
var collisionShape

# Called when the node enters the scene tree for the first time.
func _ready():
	state = states.IDLE
	sprite = $AnimatedSprite
	collisionShape = $CollisionShape2D

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	set_state(delta)
	set_flipped()

func _physics_process(delta):
	input = Vector2()
	if Input.is_action_pressed("left"):
		input.x -= 1
	if Input.is_action_pressed("right"):
		input.x += 1
	if Input.is_action_just_pressed("jump"):
		if is_on_floor():
			input.y -= 1
	process_movement(delta)

func process_movement(delta):
	var direction = 0
	inertia.y = 0
	#Calculate left / right direction
	if input.x != 0:
		direction = input.x
	else:
		if lastVelocity.x > 0:
			direction = 1
		elif lastVelocity.x < 0:
			direction = -1
		else:
			direction = 0
	#Calculate acceleration / deacceleration
	if (input.x == 0) or (input.x < 0 and lastVelocity.x > 0) or (input.x > 0 and lastVelocity.x < 0):
		if acceleration > 0:
			acceleration = 0
			inertia = lastVelocity
	else:
		if acceleration < 1:
			acceleration += delta / ACCELERATION_SPEED
			acceleration = clamp(acceleration, 0, 1)
	#Calculate residual inertia
	if inertia.x > 0:
		inertia.x -= (delta / DEACCELERATION_SPEED) * SPEED
		inertia.x = clamp(inertia.x, 0, SPEED)
	elif inertia.x < 0:
		inertia.x += (delta / DEACCELERATION_SPEED) * SPEED
		inertia.x = clamp(inertia.x, -SPEED, 0)
	#Process movement
	velocity.x = (direction * SPEED) * acceleration
	if input.y == -1:
		velocity.y = input.y * JUMP_FORCE
	elif not is_on_floor():
		velocity.y = lastVelocity.y + (GRAVITY * delta)
	else:
		velocity.y = 0
	var actualMovement = move_and_slide_with_snap(velocity + inertia, Vector2(0,1), Vector2(0,-1), true, 4, deg2rad(45), true)
	if actualMovement.y == 0:
		velocity.y = 0
	get_colliders()
	lastVelocity = velocity

func get_colliders():
	for i in get_slide_count():
		var collision = get_slide_collision(i)

func set_state(delta):
	if not is_on_floor():
		state = states.JUMPING
		sprite.play("jump")
	elif velocity.x != 0:
		state = states.WALKING
		sprite.play("run")
	else:
		state = states.IDLE
		sprite.play("idle")
		
	if Input.is_action_just_pressed("left_click"):
		state = states.THROWING
		throwTimer = throwTime
		if (global_position - get_global_mouse_position()).x < 0:
			sprite.flip_h = false
		else:
			sprite.flip_h = true
	if throwTimer > 0:
		state = states.THROWING
		throwTimer -= delta
		sprite.play("throw")

func set_flipped():
	if state != states.THROWING:
		if input.x > 0:
			sprite.flip_h = false
		if input.x < 0:
			sprite.flip_h = true
