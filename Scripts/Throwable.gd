extends RigidBody2D


# Declare member variables here. Examples:
var projectile = false
var pickup = true

# Called when the node enters the scene tree for the first time.
func _ready():
	connect("body_entered", self, "collision_detected")
	$Area2D.connect("body_entered", self, "area_entered")
	$Timer.connect("timeout", self, "allow_pickup")

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _input(event):
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT and event.is_pressed():
		if custom_integrator == true:
			var root = get_tree().get_root()
			var globalPos = global_position
			get_parent().remove_child(self)
			root.add_child(self)
			global_position = globalPos
			custom_integrator = false
			linear_velocity = -(global_position - event.global_position).normalized() * 1000
			projectile = true
			pickup = false
			$Timer.start()

func allow_pickup():
	pickup = true

func collision_detected(body):
	if body.has_method("break_tiles"):
		if projectile == true:
			body.break_tiles(global_position)

func area_entered(body):
	if pickup == true:
		call_deferred("player_carry", body)

func player_carry(body):
	var carryNode = body.get_node("Position2D")
	get_parent().remove_child(self)
	carryNode.add_child(self)
	custom_integrator = true

func _integrate_forces(state):
	if custom_integrator == true:
		position = Vector2.ZERO
		rotation = 0
		linear_velocity = Vector2.ZERO
		angular_velocity = 0
		projectile = false
