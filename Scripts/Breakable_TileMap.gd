extends TileMap

# Active nodes
var tileMap
var tileSet
# Particles settings
export var amount = 0.25
export var lifetime = 3
export var lifetime_randomness = 1
export var explosiveness = 1
export var direction = Vector2(0, 1)
export var spread = 0
export var gravity = Vector2(0, 256)
export var initial_velocity = 256
export var initial_velocity_random = 1
export var scale_amount = 4
export var scale_amount_random = 1
# Ripple effect delay
export var delay = 0.04
# Debugging
export var debug = false

# Called when the node enters the scene tree for the first time.
func _ready():
	tileMap = self
	tileSet = tileMap.tile_set

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass

func break_tiles(collisionPosition):
	var gridPoint = world_to_map(collisionPosition)
	#Pass the grid point to a function to find the connected tiles, return as var connectedTiles 
	var connectedTiles = get_connected_tiles(gridPoint)
	prepare_next_tile(connectedTiles, true)
	if debug == true:
		if connectedTiles.empty():
			print("No tile found at nearest grid point (" + str(gridPoint) + "), based on collision point of " +  str(collisionPosition))
		else:
			print("Tile found at " + str(gridPoint) + " based on collision point of " + str(collisionPosition))

func prepare_next_tile(remainingTiles, impactTile):
	var tile = remainingTiles.pop_front()
	var timer = Timer.new()
	timer.wait_time = delay
	if remainingTiles.size() > 0:
		timer.connect("timeout", self, "prepare_next_tile", [remainingTiles, false])
	if impactTile == true:
		destruct_tile(tile)
	else:
		timer.connect("timeout", self, "destruct_tile", [tile])
	timer.connect("timeout", timer, "queue_free")
	add_child(timer)
	timer.start()

func destruct_tile(tile):
	#Make sure the tile still exists
	if tile != null:
		if tileMap.get_cellv(tile) != INVALID_CELL:
			create_particle_system(tile)
			set_cellv(tile, -1, false, false, false)

func get_connected_tiles(tile):
	#Get the tiles connected to the contact tile (grid point), and loop through until no more found
	var searchRadius = Vector2(1,1)
	var allTiles = [tile] #All the tiles found
	var newTiles = search_neighbouring_tiles(searchRadius, allTiles, allTiles) #Only the tiles found in the most recent loop
	#If new tiles have been found, keep looping. Otherwise return array...
	while newTiles.size() > 0:
		allTiles.append_array(newTiles)
		newTiles = search_neighbouring_tiles(searchRadius, newTiles, allTiles)
		if debug == true:
			print("More tiles found, searching again")
	#If the impact tile was an empty tile, remove it from the array
	if get_cellv(allTiles.front()) == INVALID_CELL:
		allTiles.remove(0)
	if debug == true:
		print("Found all connected tiles, returning")
		print(allTiles)
	return allTiles

func search_neighbouring_tiles(searchRadius, passedTiles, allTiles):
	var newTiles = [] #Only the tiles found in the most recent loop
	for i in passedTiles.size():
		#Coordinates for immediately neighbouring tiles (TO THIS INDIVIDUAL TILE)
		var leftTile = Vector2(passedTiles[i].x - searchRadius.x, passedTiles[i].y)
		var upTile = Vector2(passedTiles[i].x, passedTiles[i].y - searchRadius.y)
		var rightTile = Vector2(passedTiles[i].x + searchRadius.x, passedTiles[i].y)
		var downTile = Vector2(passedTiles[i].x, passedTiles[i].y + searchRadius.y)
		var neighbouringTiles = [leftTile, upTile, rightTile, downTile]
		#Check each of the immediately neighbouring tiles to see if they're ACTIVE or EMPTY
		#Only save the ACTIVE tiles
		for tile in neighbouringTiles:
			#If cell is empty, don't save it to the list...
			if get_cellv(tile) == INVALID_CELL:
				if debug == true:
					print("Empty tile, ignore")
			#If cell is active BUT already in either list, ignore...
			elif allTiles.find(tile) != -1 or newTiles.find(tile) != -1:
				if debug == true:
					print("Repeat entry found, ignore")
			#If cell is active AND not newly discovered, add to newTiles list...
			else:
				newTiles.append(tile)
				if debug == true:
					print("Found tile, adding to newTiles")
	return newTiles

func create_particle_system(tile):
	#Get the index and texture of this tile's tileset
	var tileIndex = tileMap.get_cellv(tile)
	var texture = tileSet.tile_get_texture(tileIndex)
	var textureData = texture.get_data()
	textureData.lock()
	#Get the TEXTURE coordinates for this tile (not grid)
	var tileSprite = tileMap.get_cell_autotile_coord(tile.x, tile.y)
	#Get the size of tiles in tilemap (in pixels)
	var tileSize = tileSet.autotile_get_size(tileIndex)
	#Calculate the top-left coordinates for this tile's texture chunk
	var baseOffset = tileSprite * tileSize
	
	#Create a new particle system
	var particleSystem = CPUParticles2D.new()
	#Set the property values for the particle system
	set_particle_values(tileSize, particleSystem)
	#Create the emission mask
	var emissionMask = create_emission_mask(tileSize, baseOffset, textureData)
	particleSystem.set_emission_shape(3)
	particleSystem.set_emission_points(emissionMask["positionData"])
	particleSystem.set_emission_colors(emissionMask["colourData"])
	#Get world position for current tile and remove base offset
	particleSystem.position = map_to_world(tile) - baseOffset
	
	#Add a timer to the particle system to remove node from scene tree after lifetime ends
	var timer = Timer.new()
	timer.wait_time = lifetime
	timer.one_shot = true
	timer.autostart = true
	timer.connect("timeout", particleSystem, "queue_free")
	particleSystem.add_child(timer)
	
	#Add particle node to scene
	tileMap.add_child(particleSystem)
	#Start emitting
	particleSystem.set_one_shot(true)
	particleSystem.set_emitting(true)

	if debug == true:
		var particleDebugger = Line2D.new()
		particleDebugger.set_points([Vector2(0,0) + baseOffset, Vector2(tileSize.x,tileSize.y) + baseOffset])
		particleDebugger.default_color = Color.red
		particleSystem.add_child(particleDebugger)

func create_emission_mask(tileSize, baseOffset, textureData):
	#Get the colour of each pixel
	var colourData = PoolColorArray()
	var positionData = PoolVector2Array()
	for column in tileSize.x:
		for pixel in tileSize.y:
			var pixelData = textureData.get_pixelv(baseOffset + Vector2(column, pixel))
			#If the pixel is NOT transparent, save colour data...
			if pixelData.a != 0:
				colourData.append(pixelData)
				positionData.append(baseOffset + Vector2(column, pixel))
	#Create dictionary with emission mask values
	var emissionMask = {
		"colourData" : colourData,
		"positionData" : positionData,
	}
	return emissionMask

func set_particle_values(tileSize, particleSystem):
	#Adjust particle settings
	particleSystem.amount = (tileSize.x * tileSize.y) * amount
	particleSystem.lifetime = lifetime
	particleSystem.lifetime_randomness = lifetime_randomness
	particleSystem.explosiveness = explosiveness
	particleSystem.direction = direction
	particleSystem.spread = spread
	particleSystem.gravity = gravity
	particleSystem.initial_velocity = initial_velocity
	particleSystem.initial_velocity_random = initial_velocity_random
	particleSystem.scale_amount = scale_amount
	particleSystem.scale_amount_random = scale_amount_random
